from django.urls import path
from .views import index

# Mengalihkan URL ke method dalam views yang bersesuaian 
urlpatterns = [
    path('', index, name='index'), 
]