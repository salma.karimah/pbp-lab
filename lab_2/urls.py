from django.urls import path
from .views import index, xml_handler, json_handler, coba

# Mengalihkan URL ke method dalam views yang bersesuaian 
urlpatterns = [
    path('', index, name='index'),      
    path('coba', coba, name='coba'),          
    path('xml', xml_handler, name='xml_handler'),
    path('json', json_handler, name='json_handler'),  
]