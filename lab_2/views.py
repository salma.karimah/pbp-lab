from django.http import response
from django.shortcuts import render
from requests.sessions import Request
from .models import Note
from django.shortcuts import render
from django.http.response import HttpResponse 
from django.core import serializers
from .models import Note
import requests

# Memproses tampilan Note
def index(self):
    notes = Note.objects.all().values()                     
    response = {'notes': notes}
    return render(self, 'lab2.html', response)

# Memproses dokumen XML
def xml_handler(self):
    data = serializers.serialize('xml', Note.objects.all())                    
    return HttpResponse(data, content_type="application/xml")

# Memproses dokumen JSON
def json_handler(self):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

def coba(request):
    response = requests.get('https://api.kawalcorona.com/indonesia/provinsi')
    data = response.json()
    kasus = []

    for x in data:
        new = {}
        new['provinsi'] = x['attributes']['Provinsi']
        new['kasus_positif'] = x['attributes']['Kasus_Posi']
        new['kasus_sembuh'] = x['attributes']['Kasus_Semb']
        new['kasus_meninggal'] = x['attributes']['Kasus_Meni']
        kasus.append(new)

    response2 = requests.get('https://api.kawalcorona.com/indonesia')
    data2 = response2.json()
    if request.method == "POST":
        pilihan = request.POST['terpilih']
        for x in kasus:
            if(x['provinsi'] == pilihan):
                print(pilihan)
                print(x)
                return render(request, 'coba2.html', {'data': kasus, 'data2': data2, 'data3':x})
    return render(request, 'coba2.html', {'data': kasus, 'data2': data2})

