from django.db import models
from django.db.models.fields import CharField
from django.db import models

# Note model dengan atribut: to, from, title, dan message
class Note(models.Model):
    to = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=100)

