from django.contrib import admin
from . models import Note

# Register Note agar dapat ditambahkan di dashboard admin
admin.site.register(Note)