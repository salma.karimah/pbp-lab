import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Sign Up",
    home: BelajarForm(),
    theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.teal[600],
        fontFamily: 'Sora',
        textTheme: const TextTheme(
          headline1: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.black),
        )
      ),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController judul = TextEditingController();
  TextEditingController isi = TextEditingController();

  void allInfo() {
    AlertDialog popUp = AlertDialog(
      content: Container(
        height: 300.0,
        child: Column(
          children: <Widget>[
            Text(
              "Judul Aduan: ",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Text("${judul.text}"),
            Padding(padding: EdgeInsets.only(top: 30.0)),
            Text(
              "Pesan Aduan: ",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Text("${isi.text}")
          ],
        ),
      ),
    );
    showDialog(
      context: context, 
      builder: (_) => popUp);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,

      appBar: AppBar(
        title: Text("FEEDBACK FORM ZONA HIJAU"),
        backgroundColor: Colors.teal[600],
      ),

      body: SingleChildScrollView(
        child: Center(
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            padding: EdgeInsets.all(10.0),
            child: Center(
              child: Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(30.0)),
                  Text(
                    "GIVE YOUR FEEDBACK!",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 50.0,
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(30.0)),
                  Text(
                    "Judul: ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                  
                  TextField(
                    controller: judul,
                    decoration: InputDecoration(
                      hintText: "Judul",
                      labelText: "Judul",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0)
                      )
                    ),
                  ),

                  Padding(padding: EdgeInsets.all(30.0)),
                  Text(
                    "Pesan: ",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),

                  TextField(
                    controller: isi,
                    maxLines: 5,
                    decoration: InputDecoration(
                      hintText: "Pesan",
                      labelText: "Pesan",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0)
                      )
                    ),
                  ),

                  Padding(padding: EdgeInsets.all(30.0)),
                  RaisedButton(
                    child: Text("Submit"),
                    color: Colors.teal,
                    onPressed: (){ allInfo(); },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}