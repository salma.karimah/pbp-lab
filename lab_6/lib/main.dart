import 'package:flutter/material.dart';


/* Parent widget --> MaterialApp */
/* Scaffold() widget */
void main() { 
  runApp(
        MaterialApp(
          /* Widget Scaffold(): Membentuk aplikasi ke dalam 2 bagian: appBar + body*/
          home: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.grey[200],
            
            /* AppBar/Navigationbar dari aplikasi*/
            appBar: AppBar(
              title: Text('ZonaHijau'),
              centerTitle: true,
              backgroundColor: Colors.teal[400],
            ), // AppBar
            
            /* Main Body dari aplikasi */
            body: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 50.0, bottom: 50),
                    child: Column(
                      children: <Widget>[
                        Image.network(
                        'https://cdni.iconscout.com/illustration/premium/thumb/new-normal-at-restaurant-3594016-3007156.png',
                        width: 250,
                        height: 350,
                      ),

                      Text(
                          'ZONA HIJAU',
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 40,
                            fontWeight: FontWeight.bold,
                          ), 
                      ), 
                      Text(
                          'Zona Hijau adalah suatu web yang didesain khusus untuk membantu masyarakat Indonesia mengetahui kondisi terkait persebaran Covid-19 serta meningkatkan pemahaman pengguna terhadap virus Covid-19.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                          fontSize: 15,
                          height: 1.5,
                        ),
                      ),
                      ],
                    ),
                  ),
              
                  Container(
                    color: Colors.teal,
                    margin: const EdgeInsets.only(top: 20, bottom: 50),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(top: 30, left: 40, right: 40),
                          child: Text(
                            'COVID-19 DI INDONESIA',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),  
                        

                        Container(
                          margin: const EdgeInsets.only(left: 40, right: 40),
                          child: Text(
                            'Berikut adalah data terkini kasus Covid-19 di Indonesia beserta detail informasi kasus di tiap 34 provinsi yang ada. Ayo segera lakukan vaksinasi dan jangan lupa untuk selalu menerapkan protokol kesehatan guna menekan angka kasus Covid-19 baru di Indonesia.',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 15,
                              height: 1.5,
                            ),
                          ),
                        ),

                        Container(
                          height: 200,
                          margin: const EdgeInsets.only(left: 20, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                height: 100,
                                width: 150,
                                child: Card(
                                  margin: EdgeInsets.all(10),
                                  child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,                                
                                    children: <Widget>[
                                      Text('4.000.000'),
                                      Text('Positif'),
                                    ],
                                  ),
                                ),
                              ),

                              Container(
                                height: 100,
                                width: 150,
                        
                                child: Card(
                                  margin: EdgeInsets.all(10),                                
                                  child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text('4.000.000'),
                                      Text('Negatif'),
                                    ],
                                  ),
                                ),
                              ),

                              Container(
                                height: 100,
                                width: 150,
                                child: Card(
                                  margin: EdgeInsets.all(10),                         
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text('4.000.000'),
                                      Text('Sembuh'),
                                    ],
                                  ),
                                ),
                              ),

                              Container(
                                height: 100,
                                width: 150,
                                child: Card(
                                  margin: EdgeInsets.all(10),                              
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,                                    
                                    children: <Widget>[
                                      Text('4.000.000'),
                                      Text('Positif'),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ), // Container
                ], // Widget
              ),
            ), // children
          ), // Scaffold
        ), 
  );
}
