1. Apakah perbedaan antara JSON dan XML?

Extensible Markup Language(XML) dan JavaScript Object Notation (JSON) adalah dua bentuk format yang berfungsi sebagai pertukaran data. Namun tentu saja kedua bentuk tersebut memiliki perbedaan di beberapa hal tentu. Salah satu yang paling mendasar ialah XML meruakan suatu bahasa markup, sedangkan JSON lebih sederhana karena hanya berupa format data. Selain itu, terdapat beberapa perbedaan yang dapat membedakan keduanya:

a. Kecepatan
Dilihat dari segi kecepatan, JSON melakukan transfer data lebih cepat dibandingkan XML. Hal ini disebabkan karena seperti yang diketahui JSON merupakan bentuk format data sehingga ukuran file akan lebih kecil dibandingkan XML.

b. Bentuk Penyimpanan Data
Dalam JSON data disimpan dalam bentuk map yang memiliki suatu key dan value, sedangkan di XML data disimpan dengan bentuk tree structure

c. Kemampuan Pengolahan Data
Karena merupakan bahasa markup, XML mampu melaukan beberapa proses pengolahan data, sedangkan JSON tidak dapat melakukan pemrosesan apaun (hanya melakukan pertukaran data)

d. Jenis Penggunaan Tipe Data
JSON hanya dapat menggunakan objek yang berisi tipe data primitif, string, dan juga boolean. XML lebih banyak dapat menggunakan tipe data yang beragam dan lebih kompleks.  

2. Apakah perbedaan antara HTML dan XML?

Pada dasarnya Hypertext Markup Language (HTML) berfokus menampilkan suatu data dan mengatur tampilan dari suatu data, sedangkan Extensible Markup Language (XML) lebih berfokus kepada penyimpanan dan pertukaran dari data itu sendiri. Selain itu, berikut beberapa perbedaan yang dimiliki keduanya:

a. XML bersifat case sensitive sedangkan HTML bersifat case insetitive.

b. XML bersifat dinamis karena digunakan untuk melakukan perpindahan data dari dan ke database bukan menampilkannya. Sedangkan HTML bersifat statis karena bersifat menampilkan data

c. Di HTML closing tags tidak wajib dituliskan, sedangkan di XML harus dituliskan

d. HTML dapat mengabaikan error-error dalam skala kecil, sedangkan XML tidak memperbolehkan error dalam bentuk apapun
