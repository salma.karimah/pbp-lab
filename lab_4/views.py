from django.http import response
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponseRedirect

# Process Note Table
def index(self):
    notes = Note.objects.all().values()                     
    response = {'notes': notes}
    return render(self, 'lab4_index.html', response)

def add_note(request):
    # Create new form object
    form = NoteForm(request.POST or None)
    response = {'form':form}

    # Checking the validity of form
    if ((form.is_valid) and (request.method == 'POST')):
        form.save()
        return HttpResponseRedirect('/lab-4')
    return render(request, 'lab4_form.html', response)

def note_list(self):
    notes = Note.objects.all().values()                     
    response = {'notes': notes}
    return render(self, 'lab4_note_list.html', response)
